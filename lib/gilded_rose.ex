defmodule GildedRose do
  @moduledoc """
  Documentation for `GildedRose`.

  This module exposes `update_items/1` for updating given `Item`s as per the kata described in
  https://github.com/emilybache/GildedRose-Refactoring-Kata/blob/master/GildedRoseRequirements.txt

  ## Examples

      iex> GildedRose.update_items([%Item{name: "lettuce", sell_in: 0, quality: 50}])
      [%Item{name: "lettuce", quality: 49, sell_in: -1}]

      iex> GildedRose.update_items([%Item{name: "Aged Brie", sell_in: 0, quality: 40}])
      [%Item{name: "Aged Brie", quality: 41, sell_in: -1}]
      
      iex> GildedRose.update_items([%Item{name: "Sulfuras", sell_in: 5, quality: 40}])
      [%Item{name: "Sulfuras", quality: 80, sell_in: 5}]
  """

  def update_items(items) do
    Enum.map(items, &update_item/1)
  end

  defp update_item(%Item{} = item) do
    item
    |> update_quality
    |> update_sell_in
  end

  defp update_sell_in(%Item{name: name, sell_in: sell_in} = item) do
    if name == "Sulfuras" do
      item
    else
      %{item | sell_in: sell_in - 1}
    end
  end

  defp update_quality(%Item{name: name, sell_in: sell_in, quality: quality} = item) do
    cond do
      name == "Sulfuras" ->
        %{item | quality: 80}

      name == "Aged Brie" ->
        %{item | quality: limited_increase(quality, 1)}

      name == "Conjured" ->
        %{item | quality: limited_decrease(quality, 2)}

      name == "Backstage passes" ->
        cond do
          sell_in <= -1 ->
            %{item | quality: 0}

          sell_in <= 5 ->
            %{item | quality: limited_increase(quality, 3)}

          sell_in <= 10 ->
            %{item | quality: limited_increase(quality, 2)}

          true ->
            %{item | quality: limited_decrease(quality, 1)}
        end

      true ->
        if sell_in < 0 do
          %{item | quality: limited_decrease(quality, 2)}
        else
          %{item | quality: limited_decrease(quality, 1)}
        end
    end
  end

  defp limited_increase(quality, amount) do
    min(50, quality + amount)
  end

  defp limited_decrease(quality, amount) do
    max(0, quality - amount)
  end

end
