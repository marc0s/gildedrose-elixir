# GildedRose

This is an Elixir implementation of the [GildedRose
kata](https://github.com/emilybache/GildedRose-Refactoring-Kata/blob/master/GildedRoseRequirements.txt)

## Running

You can play around with `GildedRose.update_items/1` function straight in an
`iex` session with `% iex -S mix`:

```elixir
iex(1)> GildedRose.update_items([%Item{name: "Aged Brie", sell_in: 0, quality: 40}])
[%Item{name: "Aged Brie", quality: 41, sell_in: -1}]
iex(2)> GildedRose.update_items([%Item{name: "Sulfuras", sell_in: 5, quality: 40}]) 
[%Item{name: "Sulfuras", quality: 80, sell_in: 5}]
```

Or probably better: check that the code works by running the tests with `% mix
test --trace`.


